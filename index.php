<!DOCTYPE html>

<html>

<?php

    if(isset($_GET['file']) && !empty($_GET['file']) ){
      $menu= $_GET['file'];
    }else{
      $menu= "";
    }
    
    $menu_1 = ""; 
    $menu_2 = ""; 
    $menu_3 = "";
    $menu_4 = "";
    $menu_5 = ""; 
    $menu_6 = "";
    $menu_7 = "";
    $menu_8 = "";
    $menu_9 = "";
    $menu_10 = ""; 

    switch ($menu) {
      case "modules/view/registration.php"      : 
        $menu_1 = "active"; 
      break;
      
      case "modules/view/login.php"             : 
        $menu_2 = "active"; 
      break;
      
      case "modules/view/category.php"          : 
        $menu_3 = "active"; 
      break;
      
      case "modules/view/jasa_kirim.php"        : 
        $menu_4 = "active"; 
      break;
      
      case "modules/view/promo.php"             : 
        $menu_5 = "active"; 
      break;
      
      case "modules/view/form_buka_toko.php"              : 
        $menu_6 = "active"; 
      break;
      
      case "modules/view/form_produk_pulsa.php"   :
      case "modules/view/form_shipped_produk.php"   : 
        $menu_7 = "active"; 
      break;
      
      case "modules/view/produk_pulsa.php"      :
      case "modules/view/form_pilih_toko.php"   : 
        $menu_8 = "active"; 
      break;
      
      case "modules/view/transaksi_pulsa.php"   :
      case "modules/view/transaksi_barang.php"   : 
        $menu_9 = "active"; 
      break;
      
      case "modules/view/Keranjang_belanja.php" : 
        $menu_10 = "active"; 
      break;
    }
        
?>

  <head>

    <title>Toko Keren</title>

    <!-- Bootstrap CSS -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Jquery-->
    <script src="style/js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/jquery.dataTables.min.js"></script>
    <script src="style/js/dataTables.bootstrap.min.js"></script>

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
      <div class="navbar-header">
      <a class="navbar-brand" href="#">Toko Keren</a>
      </div>
      <ul class="nav navbar-nav">
        
        <li class="<?php echo $menu_1;?>">
          <a href="index.php?file=modules/view/registration.php">Register</a>
        </li>
        
        <li class="<?php echo $menu_2;?>">
          <a href="index.php?file=modules/view/login.php">Login</a>
        </li>
        
        <li class="<?php echo $menu_3;?>">
          <a href="index.php?file=modules/view/category.php">Kategori & Sub Kategori</a>
        </li>
        
        <li class="<?php echo $menu_4;?>">
          <a href="index.php?file=modules/view/jasa_kirim.php">Jasa Kirim</a>
        </li>
        
        <li class="<?php echo $menu_5;?>">
          <a href="index.php?file=modules/view/promo.php">Promo</a>
        </li>
        
        <li class="<?php echo $menu_6;?>">
          <a href="index.php?file=modules/view/form_buka_toko.php">Toko</a>
        </li>
        <li class="dropdown "<?php echo $menu_7;?>>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menambah Produk <strong class="caret"></strong></a>
          
          <ul class="dropdown-menu">
            <li>
              <a href="index.php?file=modules/view/form_produk_pulsa.php">Pulsa</a>
            </li>
            
            <li>
              <a href="index.php?file=modules/view/form_shipped_produk.php">Shipped</a>
            </li>

          </ul><!-- end dropdown-menu -->
        </li>
        <li class="dropdown "<?php echo $menu_8;?>>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Membeli Produk <strong class="caret"></strong></a>
          
          <ul class="dropdown-menu">
            <li>
              <a href="index.php?file=modules/view/produk_pulsa.php">Pulsa</a>
            </li>
            
            <li>
              <a href="index.php?file=modules/view/form_pilih_toko.php">Shipped</a>
            </li>

          </ul><!-- end dropdown-menu -->
        </li>
         <li class="dropdown "<?php echo $menu_9;?>>
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Transaksi <strong class="caret"></strong></a>
          
          <ul class="dropdown-menu">
            <li>
              <a href="index.php?file=modules/view/transaksi_pulsa.php">Pulsa</a>
            </li>
            
            <li>
              <a href="index.php?file=modules/view/transaksi_barang.php">Shipped</a>
            </li>

          </ul><!-- end dropdown-menu -->
        </li>
        <li class="<?php echo $menu_10;?>">
          <a href="index.php?file=modules/view/Keranjang_belanja.php">Lihat Keranjang Belanja</a>
        </li>
      </ul>
      </div>
    </nav>

    <div class="container" style="margin-top: 80px; width:90%">
      <?php 
          if(!empty($menu)){
            include $menu; 
          }else{
            include "modules/view/login.php";     
          }
      ?>  
    </div><!-- end container-->  
  </body>

</html>