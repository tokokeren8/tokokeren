
<div class="well">
	<h3 align="center">FORM MEMBUAT SHIPPED PRODUK</h3>
</div>
<div class="alert alert-success" role="alert" style="display:none">
	<strong>Peringatan!</strong>
	Message here...
</div>
<div class="container col-md-6">
	<form>
		<div class="form-group">
			<label for="InputKodeProduk">Kode Produk</label>
			<input type="text" class="form-control" placeholder="" id="kode_produk">
		</div>
		<div class="form-group">
			<label for="InputNamaProduk">Nama Produk</label>
			<input type="text" class="form-control" placeholder="" id="nama_produk">
		</div>
		<div class="form-group">
			<label for="InputHarga">Harga</label>
			<input type="number" class="form-control" placeholder="" id="harga">
		</div>
		<div class="form-group">
			<label for="InputDeskripsi">Deskripsi</label>
			<input type="text" class="form-control" placeholder="" id="deskripsi">
		</div>
		<div class="form-group">
			<label for="InputStok">Stok</label>
			<input type="number" class="form-control" placeholder="" id="stok">
		</div>
		<div class="form-group">
			<label for="InputSubKategori">Sub Kategori</label>
			<select class="form-control" id="jasaPengiriman">
				<option disabled selected value>-- Masukkan Pilihan --</option>
				<option>Pakaian</option>
				<option>Elektronik</option>
			</select>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label for="InputAsuransi">Barang Asuransi</label>
					<select class="form-control" id="isAsuransi">
						<option disabled selected value>-- Masukkan Pilihan --</option>
						<option id="ya">YA</option>
						<option id="tidak">TIDAK</option>
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="InputBaru">Barang Baru</label>
					<select class="form-control" id="isBaru" selected="tidak">
						<option disabled selected value>-- Masukkan Pilihan --</option>
						<option id="ya">YA</option>
						<option id="tidak">TIDAK</option>
					</select>
				</div>
			</div>
		</div>
		<hr/>
		<div class="form-group">
			<label for="InputMinOrder">Minimal Order</label>
			<input type="number" class="form-control" placeholder="" id="min_order">
		</div>
		<div class="form-group">
			<label for="InputMinGrosir">Minimal Grosir</label>
			<input type="number" class="form-control" placeholder="" id="min_grosir">
		</div>
		<div class="form-group">
			<label for="InputMaxGrosir">Maksimal Grosir</label>
			<input type="number" class="form-control" placeholder="" id="max_grosir">
		</div>
		<div class="form-group">
			<label for="InputHargaGrosir">Harga Grosir</label>
			<input type="number" class="form-control" placeholder="" id="harga_grosir">
		</div>
		<hr/>
		<div class="form-group">
			<label for="InputFoto">Foto</label>
			<input type="file" id="file_foto">
		</div>
		<hr/>
		<div class="form-group">
			<input type="submit" class="btn btn-success btn-block" value="Submit">
		</div>
	</form>
</div>
