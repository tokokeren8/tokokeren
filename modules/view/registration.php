<div class="container" style="width:500px;">
	<div class="row main">
		<div class="panel-heading">
		   <div class="panel-title text-center">
				<h1 class="title">FORM PENDAFTARAN PENGGUNA</h1>
				<hr />
			</div>
		</div> 
		<div class="main-login main-center">
			<form class="form-horizontal" method="post" action="#">

				<div class="form-group">
					<label for="email" class="cols-sm-2 control-label">Email</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i></span>
							<input type="text" class="form-control" name="email" id="email"  placeholder="Masukkan Email"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="password" class="cols-sm-2 control-label">Password</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock" aria-hidden="true"></i></span>
							<input type="password" class="form-control" name="password" id="password"  placeholder="Masukkan Password"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="confirm" class="cols-sm-2 control-label">Ulangi Password</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock" aria-hidden="true"></i></span>
							<input type="password" class="form-control" name="confirm" id="confirm"  placeholder="Ulangi Password"/>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="username" class="cols-sm-2 control-label">Nama Lengkap</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user" aria-hidden="true"></i></span>
							<input type="text" class="form-control" name="username" id="username"  placeholder="Masukkan Nama Lengkap"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="username" class="cols-sm-2 control-label">Nomor Telepon</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-phone" aria-hidden="true"></i></span>
							<input type="text" class="form-control" name="username" id="username"  placeholder="Masukkan Nomor Telepon"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="username" class="cols-sm-2 control-label">Alamat</label>
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-th-large" aria-hidden="true"></i></span>
							<input type="text" class="form-control" name="username" id="username"  placeholder="Masukkan Alamat"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="username" class="cols-sm-2 control-label">Jenis Kelamin</label>
					<div class="dropdown">
					<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Pilih Jenis Kelamin
					<span class="caret"></span></button>
					<ul class="dropdown-menu">
					  <li><a href="#">Laki-laki</a></li>
					  <li><a href="#">Perempuan</a></li>
					</ul>
				  </div>
				</div>

				<div class="form-group" align="center">
					<button type="button" class="btn btn-primary">Register</button>
				</div>
				<div class="login-register"  align="center">
					<a href="index.php?file=modules/view/login.php"><h4>Login</h4></a>
				 </div>
			</form>
		</div>
	</div>
</div>