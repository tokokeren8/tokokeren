
<div class="well">
	<h3 align="center">FORM MEMBUAT TOKO</h3>	
</div>
<div class="alert alert-success" role="alert" style="display:none">
	<strong>Peringatan!</strong>
	Message here...
</div>
<div class="container col-md-6">
	<form>
		<div class="form-group">
			<label for="InputNamaToko">Nama Toko</label>
			<input type="text" class="form-control" placeholder="" id="nama_toko">
		</div>
		<div class="form-group">
			<label for="InputDeskripsi">Deskripsi</label>
			<input type="text" class="form-control" placeholder="" id="dekripsi">
		</div>
		<div class="form-group">
			<label for="InputSlogan">Slogan</label>
			<input type="text" class="form-control" placeholder="" id="slogan">
		</div>
		<div class="form-group">
			<label for="InputLokasi">Lokasi</label>
			<input type="text" class="form-control" placeholder="" id="lokasi">
		</div>
		<hr/>
		<div class="form-group">
			<label for="InputJasaKirim">Jasa Kirim 1</label>
			<select class="form-control" id="jasaPengiriman">
				<option disabled selected value>-- Masukkan Pilihan --</option>
				<option>JNE</option>
				<option>TIKI</option>
			</select>
		</div>
		<div class="form-group">
			<button type="button" class="btn btn-primary btn-block">Tambah Jasa Pengiriman</button>
		</div>
		<hr/>
		<div class="form-group">
			<input type="submit" class="btn btn-success btn-block" value="Submit">
		</div>
	</form>
</div>
