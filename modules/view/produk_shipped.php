<script>
	$(document).ready(function() {
	    $('#example').DataTable();
	} );
</script>

<div class="well">
	<h3 align="center">Daftar Shipped Produk</h3>
	
    <form class="form-horizontal" style="margin-top: 50px">
    <div class="form-group">
      <label class="control-label col-sm-2" for="alamat">Kategori:</label>
      <div class="col-sm-4">
	      <select class="form-control" id="sel1">
	        <option>1</option>
	        <option>2</option>
	        <option>3</option>
	        <option>4</option>
	      </select>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="telepon">Sub Kategori:</label>
      <div class="col-sm-4"> 
	      <select class="form-control" id="sel1">
	        <option>1</option>
	        <option>2</option>
	        <option>3</option>
	        <option>4</option>
	      </select>
      </div>
    </div>
    <div class="form-group"> 
      <div class="col-sm-offset-2 col-sm-4">
        <button type="submit" class="btn btn-success">Filter</button>
      </div>
    </div>
  </form>

	<table width="100%" class="table table-striped table-bordered" style="margin-top:30px" id="example">
			<thead>
			<tr>
				<td  align ="center">
						<font color = "grey">Kode Produk
				</td>
				<td  align ="center">
						<font color = "grey">Nama Produk
				</td>
				<td  align ="center">
						<font color = "grey">Harga
				</td>
				<td  align ="center">
						<font color = "grey">Deskripsi
				</td>
				<td  align ="center">
						<font color = "grey">Is Asuransi
				</td>
				<td  align ="center">
						<font color = "grey">Stok
				</td>
				<td  align ="center">
						<font color = "grey">Is Baru
				</td>
				<td  align ="center">
						<font color = "grey">Harga Grosir
				</td>
				<td  align ="center">
						<font color = "grey">Beli
				</td>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td  align ="left">
						P0000001
				</td>
				<td  align ="left">
						Tas Flower 1
				</td>
				<td  align ="right">
						75000
				</td>
				<td  align ="left">
						KOSONG
				</td>
				<td  align ="left">
						TRUE
				</td>
				<td  align ="right">
						30
				</td>
				<td  align ="left">
						TRUE
				</td>
				<td  align ="right">
						60000
				</td>
				<td  align ="center">
					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Beli</button>
				</td>
			</tr>
			<tr>
				<td  align ="left">
						P0000002
				</td>
				<td  align ="left">
						Tas Flower 2
				</td>
				<td  align ="right">
						80000
				</td>
				<td  align ="left">
						KOSONG
				</td>
				<td  align ="left">
						TRUE
				</td>
				<td  align ="right">
						140
				</td>
				<td  align ="left">
						TRUE
				</td>
				<td  align ="right">
						70000
				</td>
				<td  align ="center">
					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Beli</button>
				</td>
			</tr>
			</tbody>
	</table>
</div><!-- end produk shipped -->




<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Pembelian</h4>
      </div>
     
     <div style="margin-top:10px">
     	<form class="form-horizontal" action="index.php?file=modules/view/Keranjang_belanja.php" method="POST">
		    <div class="form-group">
		      <label class="control-label col-sm-4" for="kode">Kode Produk:</label>
		      <div class="col-sm-4">
		        <input type="text" class="form-control" id="kode" readonly="">
		      </div>
		    </div>
		    <div class="form-group">
		      <label class="control-label col-sm-4" for="telepon">Berat Total:</label>
		      <div class="col-sm-4"> 
		        <input type="text" class="form-control" id="telepon" placeholder="Masukkan Berat">
		      </div>
		    </div>
		    <div class="form-group">
		      <label class="control-label col-sm-4" for="telepon">Jumlah Barang:</label>
		      <div class="col-sm-4"> 
		        <input type="text" class="form-control" id="telepon" placeholder="Masukkan Jumlah">
		      </div>
		    </div>
		    <div class="form-group"> 
		      <div class="col-sm-offset-4 col-sm-4">
		        <button type="submit" class="btn btn-success">Submit</button>
		      </div>
		    </div>
		  </form>
     </div>

      <div class="modal-footer"></div>
    </div>

  </div>
</div>

