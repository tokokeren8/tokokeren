<div class="container" style="width:500px;">
			<div class="panel-heading">
			   <div class="panel-title text-center">
					<h1 class="title">FORM KATEGORI DAN SUBKATEGORI</h1>
					<hr />
				</div>
			</div>
				
			<div class="main-login main-center" style="width:500px;float:center;">
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Kode kategori</label>
				  <div class="col-10">
					<input class="form-control" type="text" value="" id="example-text-input">
				  </div>
				</div>
				
				<div class="form-group row">
				  <label for="example-search-input" class="col-2 col-form-label">Nama kategori</label>
				  <div class="col-10">
					<input class="form-control" type="search" value="" id="example-search-input">
				  </div>
				</div>
				
				<div class="form-group row">
				  <label for="example-email-input" class="col-2 col-form-label" style="font-weight: 900;">Subkategori 1 :</label>
				</div>
				
				<div class="form-group row">
				  <label for="example-url-input" class="col-2 col-form-label">Kode Subkategori</label>
				  <div class="col-10">
					<input class="form-control" type="url" value="" id="example-url-input">
				  </div>
				</div>
				
				<div class="form-group row">
				  <label for="example-tel-input" class="col-2 col-form-label">Nama subkategori</label>
				  <div class="col-10">
					<input class="form-control" type="tel" value="" id="example-tel-input">
				  </div>
				</div>
				
				<div id="subkategori" name="subkategori">
				
				</div>
				
				<div class="form-group row">
				  <div class="login-register">
				       <a href="#" onclick="tambahKategori()"><h4>Tambah Subkategori</h4></a>
				  </div>
				</div>
				
				<div class="form-group" align="center">
					<button type="button" class="btn btn-primary">Submit</button>
				</div>
				
				
			</div>	
		
		</div>
		
		<script>
			var count = 2;
			function tambahKategori(){
				html = '<div class="form-group row">' +
						  '<label for="example-email-input" class="col-2 col-form-label" style="font-weight: 900;">Subkategori '+count+' : </label>' +
						'</div>' +
						
						'<div class="form-group row">' +
						  '<label for="example-url-input" class="col-2 col-form-label">Kode Subkategori</label>' +
						  '<div class="col-10">' +
							'<input class="form-control" type="url" value="" id="example-url-input">'+
						  '</div>' +
						'</div>'+
						
						'<div class="form-group row">' +
						  '<label for="example-tel-input" class="col-2 col-form-label">Nama subkategori</label>' +
						  '<div class="col-10">' +
							'<input class="form-control" type="tel" value="" id="example-tel-input">' +
						  '</div>' +
						'</div>';
						
				$("#subkategori").append(html);
				count += 1;
			}
		</script>