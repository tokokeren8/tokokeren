
<script>
	$(document).ready(function() {
	    $('#example').DataTable();
	} );
</script>

<div class="well">
	<h3 align="center">Daftar Shipped Produk</h3>
	
	<table width="100%" class="table table-striped table-bordered" style="margin-top:30px" id="example">
			<thead>
			<tr>
				<td  align ="center">
						<font color = "grey">Kode Produk
				</td>
				<td  align ="center">
						<font color = "grey">Nama Produk
				</td>
				<td  align ="center">
						<font color = "grey">Berat
				</td>
				<td  align ="center">
						<font color = "grey">Kuantitas
				</td>
				<td  align ="center">
						<font color = "grey">Harga
				</td>
				<td  align ="center">
						<font color = "grey">Sub Total
				</td>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td  align ="center">
						P0000001
				</td>
				<td  align ="center">
						Tas Flower 1
				</td>
				<td  align ="center">
						4
				</td>
				<td  align ="center">
						4
				</td>
				<td  align ="center">
						75000
				</td>
				<td  align ="center">
						300000
				</td>
			</tr>
			<tr>
				<td  align ="center">
						P0000002
				</td>
				<td  align ="center">
						Tas Flower 2
				</td>
				<td  align ="center">
						3
				</td>
				<td  align ="center">
						3
				</td>
				<td  align ="center">
						80000
				</td>
				<td  align ="center">
						240000
				</td>
			</tr>
			</tbody>
	</table>

	<form class="form-horizontal" style="margin-top: 50px">
		<div class="form-group">
			<label class="control-label col-sm-2" for="alamat">Alamat Kirim:</label>
			<div class="col-sm-4">
				<textarea class="form-control"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="jasa">Jasa Kirim:</label>
			<div class="col-sm-4"> 
				 <select class="form-control" id="jasa">
		            <option>Jasa Kirim 1</option>
		            <option>Jasa Kirim 2</option>
		            <option>Jasa Kirim 3</option>
		            <option>Jasa Kirim 4</option>
		            <option>Jasa Kirim 5</option>
		          </select>
			</div>
		</div>
		<div class="form-group"> 
			<div class="col-sm-offset-2 col-sm-4">
			<button type="submit" class="btn btn-success">Checkout</button>
			</div>
		</div>
	</form>

</div><!-- end keranjang belanja -->

