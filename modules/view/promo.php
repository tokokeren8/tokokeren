<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
</script>


<div class="col-sm-12">
  <h3 align="center">FORM MEMBUAT PROMO</h3></div>
    <form class="col-sm-6">
      <div class="form-group">
        <label for="deskripsi">Deskripsi:</label>
        <input type="text" class="form-control" id="email">
      </div>
      <div class="form-group">
        <label for="beginDate">Periode Awal:</label>
        <div class='input-group date' id='datetimepicker1'>
            <input type='date' class="form-control" />
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
      </div>
      <div class="form-group">
        <label for="endDate">Periode Akhir:</label>
        <div class='input-group date' id='datetimepicker2'>
            <input type='date' class="form-control" />
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
      </div>
      <div class="form-group">
        <label for="kode_promo">Kode Promo:</label>
        <input type="text" class="form-control" id="pwd">
      </div>
      <div class="form-group">
        <label for="kategori">Kategori:</label>
        <select class="form-control" id="sel1">
          <option>Sub kategori 1</option>
          <option>Sub kategori 2</option>
          <option>Sub kategori 3</option>
          <option>Sub kategori 4</option>
        </select>
      </div>
      <div class="form-group">
        <label for="subkategori">Sub Kategori:</label>
        <select class="form-control" id="subkategori">
          <option>Sub kategori 1</option>
          <option>Sub kategori 2</option>
          <option>Sub kategori 3</option>
          <option>Sub kategori 4</option>
        </select>
      </div>
      <button type="submit" class="btn btn-success">Submit</button>
    </form>
</div><!-- end transaksi -->

