<script>
	$(document).ready(function() {
	    $('#example').DataTable();
	} );
</script>

<div class="well">
	<h3 align="center">Daftar Transaksi Shipped</h3>
	<table width="100%" class="table table-striped table-bordered" style="margin-top:30px" id="example">
			<thead>
			<tr>
				<td  align ="center">
						<font color = "grey">Nomor Invoice
				</td>
				<td  align ="center">
						<font color = "grey">Nama Toko
				</td>
				<td  align ="center">
						<font color = "grey">Tanggal
				</td>
				<td  align ="center">
						<font color = "grey">Status
				</td>
				<td  align ="center">
						<font color = "grey">Total Bayar
				</td>
				<td  align ="center">
						<font color = "grey">Alamat
				</td>
				<td  align ="center">
						<font color = "grey">Biaya Kirim
				</td>
				<td  align ="center">
						<font color = "grey">Nomor Resi
				</td>
				<td  align ="center">
						<font color = "grey">Jasa Kirim
				</td>
				<td  align ="center">
						<font color = "grey">Ulasan 
				</td>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td  align ="left">
						V000000001
				</td>
				<td  align ="left">
						Fashion Keren 
				</td>
				<td  align ="center">
						4/1/2016 
				</td>
				<td  align ="Left">
						Barang Sudah Dibayar 
				</td>
				<td  align ="right">
						120000
				</td>
				<td  align ="left">
						Jl Pertiwi 34, Depok
				</td>
				<td  align ="right">
						25000
				</td>
				<td  align ="left">
						DPK9816788882627
				</td>
				<td  align ="left">
						JNE OKE
				</td>
				<td  align ="center">
					<a href="index.php?file=modules/view/daftar_produk_dibeli.php">DAFTAR PRODUK</a>
				</td>
			</tr>
			<tr>
				<td  align ="left">
						V000000002
				</td>
				<td  align ="left">
						Toko Buku MG 
				</td>
				<td  align ="center">
						4/1/2016 
				</td>
				<td  align ="Left">
						Transaksi Dilakukan  
				</td>
				<td  align ="right">
						25000
				</td>
				<td  align ="left">
						Jl Pertiwi 34, Depok
				</td>
				<td  align ="right">
						10000
				</td>
				<td  align ="left">
						Kosong
				</td>
				<td  align ="left">
						JNE OKE
				</td>
				<td  align ="center">
					<a href="index.php?file=modules/view/daftar_produk_dibeli.php">DAFTAR PRODUK</a>
				</td>
			</tr>

			</tbody>
	</table>
</div><!-- end transaksi -->