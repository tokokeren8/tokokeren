
<div class="well">
	<h3 align="center">FORM MEMBUAT PRODUK PULSA</h3>	
</div>
<div class="alert alert-success" role="alert" style="display:none">
	<strong>Peringatan!</strong>
	Message here...
</div>
<div class="container col-md-6">
	<form>
		<div class="form-group">
			<label for="InputKodeProduk">Kode Produk</label>
			<input type="text" class="form-control" placeholder="" id="kode_produk">
		</div>
		<div class="form-group">
			<label for="InputNamaProduk">Nama Produk</label>
			<input type="text" class="form-control" placeholder="" id="nama_produk">
		</div>
		<div class="form-group">
			<label for="InputHarga">Harga</label>
			<input type="number" class="form-control" placeholder="" id="harga">
		</div>
		<div class="form-group">
			<label for="InputDeskripsi">Deskripsi</label>
			<input type="text" class="form-control" placeholder="" id="deskripsi">
		</div>
		<div class="form-group">
			<label for="InputNominal">Nominal</label>
			<input type="number" class="form-control" placeholder="" id="nominal">
		</div>
		<hr/>
		<div class="form-group">
			<input type="submit" class="btn btn-success btn-block" value="Submit">
		</div>
	</form>
</div>
